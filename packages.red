Red[
	Title: "Redquire - list of packages"
]

http-tools #(
	name: "Red Tools"
	file: %http-tools.red
	source: https://raw.githubusercontent.com/rebolek/red-tools/master/http-tools.red
	require: [json]
)
json #(
	name: "JSON"
	file: %json.red
	source: https://raw.githubusercontent.com/rebolek/red-tools/master/json.red
)
csv #(
	name: "CSV"
	file: %csv.red
	source: https://raw.githubusercontent.com/rebolek/red-tools/master/csv.red
)
xml #(
	name: "XML"
	file: %xml.red
	source: https://raw.githubusercontent.com/rebolek/red-tools/master/xml.red
)
ansi-seq #(
	name: "ANSI sequence dialect"
	file: %ansi-seq.red
	source: https://raw.githubusercontent.com/rebolek/red-tools/master/ansi-seq.red
)